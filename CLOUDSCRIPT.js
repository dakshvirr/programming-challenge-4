handlers.updateCaughtPokemonCount = function (args, context) {
    var request = {
        PlayFabId: currentPlayerId, Statistics: [{
                StatisticName: "CaughtPokemon",
                Value: args.mCaughtPokemon
            }]
    };
    var playerStatResult = server.UpdatePlayerStatistics(request);
};
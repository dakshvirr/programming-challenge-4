﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using PlayFab.ClientModels;

public class GameMenu : Menu
{
    [SerializeField] TMP_InputField mDisplayNameText;
    [SerializeField] TMP_InputField mPokeIndex;
    [SerializeField] Button mChangeDisplayNameBtn;
    [SerializeField] TextMeshProUGUI mCaughtPokemonCount;
    [SerializeField] Sprite mDefaultPokeSprite;
    [SerializeField] Image mPokeRenderer;
    [SerializeField] TextMeshProUGUI mPokeName;
    [SerializeField] Button mCatchPokemonBtn;
    
    [SerializeField] Button mLeaderboardBtn;
    [SerializeField] RectTransform mLeaderBoardContent;
    [SerializeField] HorizontalLayoutGroup mLeaderBoardHG;
    [SerializeField] GameObject mLeaderboardPrefab;
    [SerializeField] string mDefaultPokemonName = "Invalid Pokemon";
    override protected void OnVisible()
    {
        SetDefaultPokemonDisplay();
        GameManager.Instance.mPokeAPIManager.mOnPokemonUpdated.AddListener(SetPokemonDisplay);
        if(!string.IsNullOrEmpty(GameManager.Instance.mMainPlayer.mDisplayName))
        {
            mDisplayNameText.text = GameManager.Instance.mMainPlayer.mDisplayName;
        }
        mCaughtPokemonCount.text = GameManager.Instance.mMainPlayer.mCaughtPokemonCount.ToString();
        mChangeDisplayNameBtn.interactable = 
            !string.IsNullOrEmpty(mDisplayNameText.text)
            && !mDisplayNameText.text.Equals(GameManager.Instance.mMainPlayer.mDisplayName);
    }

    void OnDisable()
    {
        if(GameManager.IsValidSingleton())
        {
            if(GameManager.Instance.mPokeAPIManager != null)
            {
                GameManager.Instance.mPokeAPIManager.mOnPokemonUpdated.RemoveListener(SetPokemonDisplay);
            }
        }
    }

    public void OnDisplayNameTextChanged(string pText)
    {
        mChangeDisplayNameBtn.interactable = !string.IsNullOrEmpty(mDisplayNameText.text)
            && !mDisplayNameText.text.Equals(GameManager.Instance.mMainPlayer.mDisplayName);
    }

    public void ChangeDisplayName()
    {
        mChangeDisplayNameBtn.interactable = false;
        GameManager.Instance.mMainPlayer.mDisplayName = mDisplayNameText.text;
        GameManager.Instance.mPlayFabManager.UpdatePlayerName();
    }

    public void OnPokeIndexChanged(string pText)
    {
        int aPokeIndex;
        if(!int.TryParse(mPokeIndex.text,out aPokeIndex))
        {
            GameManager.Instance.mPokeAPIManager.FetchPokeDetails(-1);
            return;
        }
        GameManager.Instance.mPokeAPIManager.FetchPokeDetails(aPokeIndex);
    }


    void SetPokemonDisplay(string pPokemonName, byte[] pPokemonImage)
    {
        if(string.IsNullOrEmpty(pPokemonName))
        {
            SetDefaultPokemonDisplay();
            return;
        }
        mPokeName.text = pPokemonName;
        Texture2D aNewTexture = new Texture2D(0,0);
        if(!aNewTexture.LoadImage(pPokemonImage))
        {
            SetDefaultPokemonDisplay();
            return;
        }
        mPokeRenderer.sprite = Sprite.Create(aNewTexture, new Rect(0, 0, aNewTexture.width, aNewTexture.height), Vector2.one * 0.5f);
        mCatchPokemonBtn.interactable = true;
    }
    
    public void SetDefaultPokemonDisplay(string pChangedTxt = "")
    {
        mPokeName.text = mDefaultPokemonName;
        mPokeRenderer.sprite = mDefaultPokeSprite;
        mCatchPokemonBtn.interactable = false;
    }


    public void CatchPokemon()
    {
        GameManager.Instance.CatchPokemon();
        mCaughtPokemonCount.text = GameManager.Instance.mMainPlayer.mCaughtPokemonCount.ToString();
    }

    public void PopulateLeaderboard()
    {
        mLeaderboardBtn.interactable = false;
        foreach(RectTransform aChild in mLeaderBoardContent)
        {
            mLeaderBoardContent.sizeDelta -= new Vector2(mLeaderBoardHG.spacing + aChild.sizeDelta.x, 0);
            Destroy(aChild.gameObject);
        }
        GameManager.Instance.mPlayFabManager.GetLeaderboardResult(5, OnGetLeaderboardResult);
    }

    void OnGetLeaderboardResult(GetLeaderboardResult pResult)
    {
        foreach (PlayerLeaderboardEntry aEntry in pResult.Leaderboard)
        {
            GameObject aLeaderboardInst = Instantiate(mLeaderboardPrefab, mLeaderBoardContent, false);
            LeaderboardUI aUI = aLeaderboardInst.GetComponent<LeaderboardUI>();
            mLeaderBoardContent.sizeDelta += new Vector2(mLeaderBoardHG.spacing + aUI.mSelfTransform.sizeDelta.x, 0);
            aUI.mDisplayName.text = aEntry.DisplayName;
            aUI.mStatisticsValue.text = aEntry.StatValue.ToString();
        }
        mLeaderboardBtn.interactable = true;
    }


}

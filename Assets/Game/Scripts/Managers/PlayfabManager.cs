﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class PlayfabManager : MonoBehaviour
{

    public enum LoginState
    {
        None = -1,
        RequestSent,
        Success,
        Failure
    }

    [HideInInspector] public LoginState mLoginState = LoginState.None;
    public readonly UnityEvent mOnLogin = new UnityEvent();
    bool mInit = false;
    PlayFabAuthenticationContext mCurrentUserSession;

    public void Init()
    {
        if(mInit)
        {
            return;
        }
        if(mLoginState != LoginState.None)
        {
            return;
        }
        LoginWithCustomIDRequest aLoginRequest = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true
        };
        mLoginState = LoginState.RequestSent;
        PlayFabClientAPI.LoginWithCustomID(aLoginRequest, OnLoginSucceeded, OnError);
        mInit = true;
    }

    void OnLoginSucceeded(LoginResult pResult)
    {
        mLoginState = LoginState.Success;
        mCurrentUserSession = pResult.AuthenticationContext;
        PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest
        {
            AuthenticationContext = mCurrentUserSession
        }, OnGetAccountInfo, OnError);
        PlayFabClientAPI.GetPlayerStatistics(
            new GetPlayerStatisticsRequest
            {
                AuthenticationContext = mCurrentUserSession,
                StatisticNames = new List<string>
                {
                    GameManager.Instance.mCghtPokeCntStatName
                }
            },
            OnGetPlayerStatistics, OnError);
    }

    void OnGetAccountInfo(GetAccountInfoResult pResult)
    {
        GameManager.Instance.mMainPlayer.mDisplayName = pResult.AccountInfo.TitleInfo.DisplayName;
        mOnLogin.Invoke();
    }

    void OnGetPlayerStatistics(GetPlayerStatisticsResult pResult)
    {
        foreach(var aStatisticValue in pResult.Statistics)
        {
            if(aStatisticValue.StatisticName.Equals(GameManager.Instance.mCghtPokeCntStatName))
            {
                GameManager.Instance.mMainPlayer.mCaughtPokemonCount = aStatisticValue.Value;
            }
        }
    }

    void OnError(PlayFabError pError)
    {
        Debug.LogErrorFormat("Error Occured :- {0}\n{1}\n", pError.ErrorMessage, pError.ErrorDetails.ToString());
    }

    public void UpdatePlayerName()
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(
            new UpdateUserTitleDisplayNameRequest() 
            { 
                AuthenticationContext = mCurrentUserSession,
                DisplayName = GameManager.Instance.mMainPlayer.mDisplayName 
            },
            (pResult) => { },
            OnError);
    }

    public void GetLeaderboardResult(int pCount, System.Action<GetLeaderboardResult> pResultCallback)
    {
        PlayFabClientAPI.GetLeaderboard(
            new GetLeaderboardRequest()
            {
                AuthenticationContext = mCurrentUserSession,
                MaxResultsCount = pCount,
                StartPosition = 0,
                StatisticName = GameManager.Instance.mCghtPokeCntStatName
            }, pResultCallback, OnError);
    }

    public void UpdateStatistics()
    {
        PlayFabClientAPI.ExecuteCloudScript(
            new ExecuteCloudScriptRequest()
            {
                AuthenticationContext = mCurrentUserSession,
                FunctionName = "updateCaughtPokemonCount",
                FunctionParameter = new { mCaughtPokemon = GameManager.Instance.mMainPlayer.mCaughtPokemonCount }
            },
            (pResult) => { },
            OnError);

    }

    public void SendCatchPokemonAnalyticsEvent(CaughtPokemonEventData pData)
    {
        PlayFabClientAPI.WritePlayerEvent(
            new WriteClientPlayerEventRequest()
            {
                AuthenticationContext = mCurrentUserSession,
                EventName = "player_caught_pokemon",
                Timestamp = System.DateTime.Now,
                Body = new Dictionary<string, object>
                {
                    { "CaughPokemonEventData" , pData }
                }
            },
            (pResult) => { },
            OnError);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class PokemonUpdatedEvent : UnityEvent<string,byte[]> { }

[System.Serializable]
public struct PokemonSprites
{
    public string front_default;
    //public string front_shiny;
    //public string front_female;
    //public string front_shiny_female;
    //public string back_default;
    //public string back_shiny;
    //public string back_female;
    //public string back_shiny_female;
}

[System.Serializable]
public class Pokemon
{
    public int id;
    public string name;
    public PokemonSprites sprites;
}
public class PokeAPIManager : MonoBehaviour
{
    bool mInit = false;
    [HideInInspector] public readonly PokemonUpdatedEvent mOnPokemonUpdated = new PokemonUpdatedEvent();
    [HideInInspector] public Pokemon mActivePokemon;
    [HideInInspector] public byte[] mPokeTexture;
    [SerializeField] string mPokeAPIURI = "https://pokeapi.co/api/v2/pokemon/";
    bool mRequestSent = false;
    public void Init()
    {
        if (mInit)
        {
            return;
        }
        mActivePokemon = null;
        mPokeTexture = null;
        mInit = true;
    }



    public void FetchPokeDetails(int pIndex)
    {
        if(pIndex == -1)
        {
            mActivePokemon = null;
            mOnPokemonUpdated.Invoke(null, null);
            return;
        }
        if(mRequestSent)
        {
            mRequestSent = false;
            mActivePokemon = null;
            StopAllCoroutines();
        }
        if(mActivePokemon != null)
        {
            if (mActivePokemon.id == pIndex)
            {
                mOnPokemonUpdated.Invoke(mActivePokemon.name, mPokeTexture);
                return;
            }
            mActivePokemon = null;
        }
        mRequestSent = true;
        StartCoroutine(GetAPIResponse(pIndex));
    }

    IEnumerator GetAPIResponse(int pIndex)
    {
        UnityWebRequest aRequest = UnityWebRequest.Get(string.Format("{0}{1}/", mPokeAPIURI, pIndex));
        aRequest.SendWebRequest();
        yield return new WaitUntil(() => aRequest.isDone);
        if(aRequest.isHttpError || aRequest.isNetworkError)
        {
            mOnPokemonUpdated.Invoke(null, null);
            yield break;
        }
        mActivePokemon = new Pokemon();
        mActivePokemon = JsonUtility.FromJson<Pokemon>(aRequest.downloadHandler.text);
        if(mActivePokemon == null)
        {
            mOnPokemonUpdated.Invoke(null, null);
            yield break;
        }
        aRequest = UnityWebRequest.Get(mActivePokemon.sprites.front_default);
        aRequest.SendWebRequest();
        yield return new WaitUntil(() => aRequest.isDone);
        if (aRequest.isHttpError || aRequest.isNetworkError)
        {
            mOnPokemonUpdated.Invoke(null, null);
            yield break;
        }
        mPokeTexture = aRequest.downloadHandler.data;
        mOnPokemonUpdated.Invoke(mActivePokemon.name, mPokeTexture);
        mRequestSent = false;
    }

}

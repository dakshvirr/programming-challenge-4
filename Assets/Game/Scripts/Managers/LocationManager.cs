﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour
{
    [SerializeField] float mLocationUpdateInterval = 30.0f;
    float mCurrentTimer = 0.0f;
    [SerializeField] bool mDebugInstant = false;
    bool mServiceCalled = false;
    [HideInInspector] public float mLatitude;
    [HideInInspector] public float mLongitude;
    public void Init()
    {
        GetLocation();
    }

    void Update()
    {
        if (mServiceCalled)
        {
            return;
        }
        mCurrentTimer += Time.deltaTime;
        if(mCurrentTimer >= mLocationUpdateInterval || mDebugInstant)
        {
            mCurrentTimer = 0.0f;
            GetLocation();
        }
    }

    void GetLocation()
    {
        mServiceCalled = true;
        StartCoroutine(GetUserLocation());
    }

    IEnumerator GetUserLocation()
    {
        Input.location.Start();
        int aWaitTime = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && aWaitTime > 0)
        {
            yield return new WaitForSeconds(1);
            aWaitTime--;
        }
        if (aWaitTime <= 0)
        {
            yield break;
        }
        if (Input.location.status == LocationServiceStatus.Failed || Input.location.status == LocationServiceStatus.Stopped)
        {
            yield break;
        }
        mLatitude = Input.location.lastData.latitude;
        mLongitude = Input.location.lastData.longitude;
        Input.location.Stop();
        mServiceCalled = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Player
{
    public string mDisplayName;
    public int mCaughtPokemonCount;
}

[System.Serializable]
public struct CaughtPokemonEventData
{
    public string mPokemonName;
    public float mLatitude;
    public float mLongitude;
}

public class GameManager : Singleton<GameManager>
{
    public PokeAPIManager mPokeAPIManager;
    public PlayfabManager mPlayFabManager;
    public LocationManager mLocationManager;
    [HideInInspector]public Player mMainPlayer;
    public MenuClassifier mGameScene;

    public string mCghtPokeCntStatName = "CaughtPokemon";

    void Start()
    {
        mPlayFabManager.mOnLogin.AddListener(OnLoginSuccess);
        mLocationManager.Init();
        mPlayFabManager.Init();
        mPokeAPIManager.Init();
    }

    void OnDestroy()
    {
        if(mPlayFabManager != null)
        {
            mPlayFabManager.mOnLogin.RemoveListener(OnLoginSuccess);
        }
    }


    void OnLoginSuccess()
    {
        MenuManager.Instance.HideLoad();
        MenuManager.Instance.ShowMenu(mGameScene);
    }

    public void CatchPokemon()
    {
        mMainPlayer.mCaughtPokemonCount++;
        mPlayFabManager.UpdateStatistics();
        CaughtPokemonEventData aCatchPokeData = new CaughtPokemonEventData();
        aCatchPokeData.mPokemonName = mPokeAPIManager.mActivePokemon.name;
        aCatchPokeData.mLatitude = mLocationManager.mLatitude;
        aCatchPokeData.mLongitude = mLocationManager.mLongitude;
        mPlayFabManager.SendCatchPokemonAnalyticsEvent(aCatchPokeData);
    }


}

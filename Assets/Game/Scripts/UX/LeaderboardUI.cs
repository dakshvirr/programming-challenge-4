﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LeaderboardUI : MonoBehaviour
{
    public RectTransform mSelfTransform;
    public TextMeshProUGUI mDisplayName;
    public TextMeshProUGUI mStatisticsValue;
}
